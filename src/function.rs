use crate::stmt::{Stmt, Function};
use crate::callable::{Callable, CallableTrait};
use crate::interpreter::{Interpreter, RuntimeError};
use crate::token_literal_type::TokenLiteralType;
use crate::environment::Environment;
use std::cell::RefCell;
use std::rc::Rc;
use std::hash::{Hash, Hasher};

#[derive(Debug, Clone, Eq)]
pub struct LoxFunction {
    declaration: Function,
    closure_environment: Rc<RefCell<Environment>>,
}

impl Hash for LoxFunction {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.declaration.hash(state);
    }
}

impl PartialEq for LoxFunction {
    fn eq(&self, other: &LoxFunction) -> bool {
        false
    }
}


impl<'a> LoxFunction {
    pub(crate) fn new(declaration: Function, closure_environment: Rc<RefCell<Environment>>) -> Self {
        Self {
            declaration,
            closure_environment
        }
    }
}

impl CallableTrait for LoxFunction {
    fn call(&mut self, interpreter: &mut Interpreter, arguments: Vec<TokenLiteralType>) -> Result<TokenLiteralType, RuntimeError> {
        let environment = Rc::new(RefCell::new(Environment::new(Some(self.closure_environment.clone()))));
        for i in 0..self.declaration.params.len() {
            environment.borrow_mut().define(self.declaration.params[i].lexeme.clone(), arguments[i].clone());
        }
        match interpreter.execute_block(&self.declaration.body, environment) {
            Ok(_) => (),
            Err(e) => return match e.return_value {
                Some(val) => Ok(val),
                None => Err(e)
            }
        };
        Ok(TokenLiteralType::Nil)
    }

    fn arity(&self) -> usize {
        self.declaration.params.len()
    }
}


