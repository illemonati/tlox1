

#[macro_use]
extern crate derive_new;
#[macro_use]
extern crate derive_more;


mod lox;
mod token_type;
mod token;
mod scanner;
mod expr;
mod parser;
mod token_literal_type;
mod interpreter;
mod stmt;
mod environment;
mod callable;
mod function;
mod resolver;

use std::{env, process};
use lox::Lox;
// use crate::expr::{Expr, Binary, Unary, Literal, Grouping};
// use crate::token::{Token};
// use crate::token_literal_type::TokenLiteralType;
use crate::token_type::TokenType;


fn main() {
    let args: Vec<String> = env::args().collect();
    // test_ast_print();
    if args.len() > 2 {
        println!("Usage tlox1 [script]");
        process::exit(64);
    } else if args.len() == 2 {
        Lox::new().run_file(&args[1]);
    } else {
        Lox::new().run_prompt();
    }
}



// fn test_ast_print() {
//     let expr = Expr::Binary(Box::new(Binary::new(
//         Expr::Unary(Box::new(Unary::new(
//             Token::new(TokenType::MINUS, "-", TokenLiteralType::Nil, 1),
//             Expr::Literal(Box::new(Literal::new(TokenLiteralType::Number(123.0))))
//         ))),
//         Token::new(TokenType::STAR, "*", TokenLiteralType::Nil, 1),
//         Expr::Grouping(Box::new(Grouping::new(Expr::Literal(Box::new(Literal::new(TokenLiteralType::Number(45.67)))))))
//     )));
//     println!("{}", expr);
// }






