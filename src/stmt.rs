use crate::expr::Expr;
use crate::interpreter::{Evaluatable, RuntimeError, Interpreter};
use crate::callable::{Callable};
use crate::token_literal_type::TokenLiteralType;
use crate::token::Token;
use std::fmt::{Display, Formatter};
use std::fmt;
use std::io;
use std::io::Write;
use crate::environment::Environment;
use crate::function::LoxFunction;
use std::cell::RefCell;
use std::rc::Rc;
use crate::expr::Expr::Literal;
use crate::resolver::{Resolvable, Resolver, ResolutionError, FunctionType};

#[derive(Display, Debug, Clone, Hash, PartialEq, Eq)]
pub enum Stmt {
    Expression(Box<Expression>),
    Print(Box<Print>),
    Var(Box<Var>),
    Block(Box<Block>),
    If(Box<If>),
    While(Box<While>),
    Function(Box<Function>),
    Return(Box<Return>),
}

impl Evaluatable for Stmt {
    fn evaluate(&self,  i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        match self {
            Self::Expression(e) => e.evaluate(i),
            Self::Print(p) => p.evaluate(i),
            Self::Var(v) => v.evaluate(i),
            Self::Block(b) => b.evaluate(i),
            Self::If(ie) => ie.evaluate(i),
            Self::While(w) => w.evaluate(i),
            Self::Function(f) => f.evaluate(i),
            Self::Return(r) => r.evaluate(i),
        }
    }
}

impl Resolvable for Stmt {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        match self {
            Self::Block(b) => b.resolve(resolver),
            Self::Var(v) => v.resolve(resolver),
            Self::Function(f) => f.resolve(resolver),
            Self::Expression(e) => e.resolve(resolver),
            Self::If(i) => i.resolve(resolver),
            Self::While(w) => w.resolve(resolver),
            Self::Return(r) => r.resolve(resolver),
            Self::Print(p) => p.resolve(resolver)
        }
    }
}




#[derive(new, Display, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Expression {
    expression: Expr,
}

impl Evaluatable for Expression {
    fn evaluate(&self, i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        self.expression.evaluate(i)?;
        Ok(TokenLiteralType::Nil)
    }
}
impl Resolvable for Expression {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.expression.resolve(resolver
        )
    }
}


#[derive(new, Display, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Print {
    expression: Expr,
}

impl Evaluatable for Print {
    fn evaluate(&self,  i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let val = self.expression.evaluate(i)?;
        println!("{}", val);
        let _ = io::stdout().flush();
        Ok(TokenLiteralType::Nil)
    }
}
impl Resolvable for Print {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.expression.resolve(resolver)
    }
}

#[derive(new, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Var {
    name: Token,
    initializer: Expr
}

impl Evaluatable for Var {
    fn evaluate(&self,  i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let value = self.initializer.evaluate(i)?;
        i.environment.borrow_mut().define(self.name.lexeme.clone(), value);
        Ok(TokenLiteralType::Nil)
    }
}

impl Resolvable for Var {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        resolver.declare(&self.name)?;
        self.initializer.resolve(resolver)?;
        resolver.define(&self.name);
        Ok(())
    }
}

impl Display for Var {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.name.lexeme)
    }
}

#[derive(new, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Block {
    statements: Vec<Stmt>
}

impl Display for Block {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", "{")?;
        for stmt in &self.statements {
            write!(f, "{}", stmt)?;
        }
        write!(f, "{}", "}")
    }
}

impl Evaluatable for Block {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let new_environment = Rc::new(RefCell::new(Environment::new(Some(interpreter.environment.clone()))));
        interpreter.execute_block(&self.statements, new_environment)?;
        Ok(TokenLiteralType::Nil)
    }
}

impl Resolvable for Block {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        resolver.begin_scope();
        resolver.resolve_block(&self.statements)?;
        resolver.end_scope();
        Ok(())
    }
}


#[derive(new, Debug, Clone, Hash, PartialEq, Eq)]
pub struct If {
    condition: Expr,
    then_branch: Stmt,
    else_branch: Stmt
}

impl Evaluatable for If {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        if self.condition.evaluate(interpreter)?.is_truthy() {
            self.then_branch.evaluate(interpreter)?;
        } else {
            self.else_branch.evaluate(interpreter)?;
        }
        return Ok(TokenLiteralType::Nil);
    }
}
impl Resolvable for If {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.condition.resolve(resolver)?;
        self.then_branch.resolve(resolver)?;
        self.else_branch.resolve(resolver)?;
        Ok(())
    }
}


impl Display for If {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "if {} {} {}", self.condition, self.then_branch, self.else_branch)
    }
}



#[derive(new, Debug, Clone, Hash, PartialEq, Eq)]
pub struct While {
    condition: Expr,
    body: Stmt,
}

impl Evaluatable for While {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        while self.condition.evaluate(interpreter)?.is_truthy() {
            self.body.evaluate(interpreter)?;
        }
        Ok(TokenLiteralType::Nil)
    }
}
impl Resolvable for While {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.condition.resolve(resolver)?;
        self.body.resolve(resolver)?;
        Ok(())
    }
}


impl Display for While {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "while {} {}", self.condition, self.body)
    }
}


#[derive(new, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Function {
    pub name: Token,
    pub params: Vec<Token>,
    pub body: Vec<Stmt>
}


impl Display for Function {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "function {}", self.name)
    }
}

impl Evaluatable for Function {
    fn evaluate(&self, interpreter: &mut Interpreter) ->  Result<TokenLiteralType, RuntimeError> {
        let function = LoxFunction::new(self.clone(), interpreter.environment.clone());
        interpreter.environment.borrow_mut().define(self.name.lexeme.clone(), TokenLiteralType::Callable(Callable::Function(Box::new(function))));
        Ok(TokenLiteralType::Nil)
    }
}
impl Resolvable for Function {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        resolver.declare(&self.name)?;
        resolver.define(&self.name);
        resolver.resolve_function(self.clone(), FunctionType::Function)?;
        Ok(())
    }
}

#[derive(new, Debug, Clone, Hash, PartialEq, Eq)]
pub struct Return {
    keyword: Token,
    value: Expr
}

impl Display for Return {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "return")
    }
}

impl Evaluatable for Return {
    fn evaluate(&self, interpreter: &mut Interpreter) ->  Result<TokenLiteralType, RuntimeError> {
        let value = self.value.evaluate(interpreter)?;
        Err(RuntimeError::new_return(self.keyword.clone(), "return", value))
    }
}
impl Resolvable for Return {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        if resolver.current_function_type == FunctionType::None {
            return Err(ResolutionError::new(self.keyword.clone(), "Cannot return from top-level code."))
        }
        self.value.resolve(resolver)
    }
}


