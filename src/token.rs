use crate::token_type::TokenType;
use std::fmt::{self, Display, Formatter, Debug};
use crate::token_literal_type::TokenLiteralType;


#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct Token {
    pub token_type: TokenType,
    pub lexeme: String,
    pub literal: TokenLiteralType,
    pub line: usize,
}

impl Token {
    pub fn new(token_type: TokenType, lexeme: impl Into<String>, literal: TokenLiteralType, line: usize) -> Self {
        Self {
            token_type,
            lexeme: lexeme.into(),
            literal,
            line
        }
    }
}


impl Display for Token {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self.lexeme)
    }
}




