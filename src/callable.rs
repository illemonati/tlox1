use crate::interpreter::{Interpreter, RuntimeError};
use crate::token_literal_type::TokenLiteralType;
use std::cell::RefCell;
use std::rc::Rc;
use std::fmt;
use crate::function::LoxFunction;


#[derive(Clone, Debug, Hash, Eq)]
pub enum Callable {
    Function(Box<LoxFunction>)
}

impl PartialEq for Callable {
    fn eq(&self, other: &Self) -> bool {
        false
    }
}

impl CallableTrait for Callable {
    fn call(&mut self, interpreter: &mut Interpreter, arguments: Vec<TokenLiteralType>) -> Result<TokenLiteralType, RuntimeError> {
        match self {
            Self::Function(f) => f.call(interpreter, arguments),
        }
    }
    fn arity(&self) -> usize {
        match self {
            Self::Function(f) => f.arity(),
        }
    }
}



// impl PartialEq for CallableWrapper {
//     fn eq(&self, other: &Self) -> bool {
//         false
//     }
// }
//
// impl Callable for CallableWrapper {
//     fn call(&mut self, interpreter: &mut Interpreter, arguments: Vec<TokenLiteralType>) -> Result<TokenLiteralType, RuntimeError> {
//         println!("borrow callable 22");
//         self.inner.call(interpreter, arguments)
//     }
//     fn arity(&self) -> usize {
//         self.inner.arity()
//     }
// }
//
//
// impl fmt::Debug for CallableWrapper {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
//         write!(f, "callable")
//     }
// }
//
//
pub trait CallableTrait {
    fn call(&mut self, interpreter: &mut Interpreter, arguments: Vec<TokenLiteralType>) -> Result<TokenLiteralType, RuntimeError>;

    fn arity(&self) -> usize;
}


