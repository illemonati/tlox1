use crate::token::{Token};
use std::fmt::{Display, Formatter};
use std::fmt;
use crate::token_type::TokenType;
use crate::token_literal_type::TokenLiteralType;
use crate::interpreter::{Evaluatable, RuntimeError, Interpreter};
use crate::callable::Callable;
use crate::callable::CallableTrait;
use crate::resolver::{Resolvable, Resolver, ResolutionError};

macro_rules! parenthesize {
    ($name: expr, $( $x: expr ),*) => {{
        let mut tmp_str = format!("({}", $name);
        $(
            tmp_str.push_str(" ");
            tmp_str.push_str(&format!("{}", $x));
        )*
        tmp_str.push_str(")");
        tmp_str
    }}
}

#[derive(Display, Debug, Clone, Hash, Eq, PartialEq)]
pub enum Expr {
    Binary(Box<Binary>),
    Grouping(Box<Grouping>),
    Literal(Box<Literal>),
    Unary(Box<Unary>),
    Variable(Box<Variable>),
    Assign(Box<Assign>),
    Logical(Box<Logical>),
    Call(Box<Call>),
}

impl Evaluatable for Expr {
    fn evaluate(&self, i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError>{
        match self {
            Self::Binary(n) => n.evaluate(i),
            Self::Grouping(n) => n.evaluate(i),
            Self::Literal(n) => n.evaluate(i),
            Self::Unary(n) => n.evaluate(i),
            Self::Variable(n) => n.evaluate(i),
            Self::Assign(n) => n.evaluate(i),
            Self::Logical(n) => n.evaluate(i),
            Self::Call(n) => n.evaluate(i),
        }
    }
}

impl Resolvable for Expr {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        match self {
            Self::Variable(v) => v.resolve(resolver),
            Self::Assign(a) => a.resolve(resolver),
            Self::Binary(b) => b.resolve(resolver),
            Self::Grouping(g) => g.resolve(resolver),
            Self::Literal(l) => l.resolve(resolver),
            Self::Unary(u) => u.resolve(resolver),
            Self::Logical(l) => l.resolve(resolver),
            Self::Call(c) => c.resolve(resolver),
        }
    }
}


#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Binary {
    left: Expr,
    operator: Token,
    right: Expr,
}

impl Display for Binary {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", parenthesize!(self.operator, self.left, self.right))
    }
}

impl Evaluatable for Binary {
    fn evaluate(&self, i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let left = self.left.evaluate(i)?;
        let right = self.right.evaluate(i)?;
        if let TokenLiteralType::Number(left_num) = left {
            if let TokenLiteralType::Number(right_num) = right {
                return match self.operator.token_type {
                    TokenType::MINUS => Ok(TokenLiteralType::Number(left_num-right_num)),
                    TokenType::PLUS => Ok(TokenLiteralType::Number(left_num+right_num)),
                    TokenType::SLASH => Ok(TokenLiteralType::Number(left_num/right_num)),
                    TokenType::STAR => Ok(TokenLiteralType::Number(left_num*right_num)),
                    TokenType::GREATER => Ok(TokenLiteralType::Bool(left_num>right_num)),
                    TokenType::GREATER_EQUAL => Ok(TokenLiteralType::Bool(left_num >= right_num)),
                    TokenType::LESS_EQUAL => Ok(TokenLiteralType::Bool(left_num <= right_num)),
                    TokenType::LESS => Ok(TokenLiteralType::Bool(left_num < right_num)),
                    TokenType::EQUAL_EQUAL => Ok(TokenLiteralType::Bool(left_num == right_num)),
                    TokenType::BANG_EQUAL => Ok(TokenLiteralType::Bool(!(left_num != right_num))),
                    _ => Err(RuntimeError::new(self.operator.clone(), "Invalid operator")),
                }
            }
        }
        if let TokenLiteralType::String(ref left_str) = left {
            if let TokenLiteralType::String(ref right_str) = right {
                return match self.operator.token_type {
                    TokenType::PLUS => Ok(TokenLiteralType::String(format!("{}{}", left_str, right_str))),
                    _ => Err(RuntimeError::new(self.operator.clone(), "Invalid operator")),
                }
            }
        }

        return match self.operator.token_type {
            TokenType::EQUAL_EQUAL => Ok(TokenLiteralType::Bool(left.is_equal(right))),
            TokenType::BANG_EQUAL => Ok(TokenLiteralType::Bool(!(left.is_equal(right)))),
            _ => Err(RuntimeError::new(self.operator.clone(), "Invalid operator")),
        };
    }
}
impl Resolvable for Binary {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.left.resolve(resolver)?;
        self.right.resolve(resolver)?;
        Ok(())
    }
}


#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Grouping {
    expression: Expr,
}

impl Display for Grouping {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", parenthesize!("group", self.expression))
    }
}
impl Evaluatable for Grouping {
    fn evaluate(&self, i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        self.expression.evaluate(i)
    }
}
impl Resolvable for Grouping {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError>  {
        self.expression.resolve(resolver)
    }
}


#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Literal {
    value: TokenLiteralType
}

impl Display for Literal {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl Evaluatable for Literal {
    fn evaluate(&self, _: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        Ok(self.value.clone())
    }
}

impl Resolvable for Literal {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError>  {
        Ok(())
    }
}


#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Unary {
    operator: Token,
    right: Expr,
}

impl Display for Unary {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", parenthesize!(self.operator.lexeme, self.right))
    }
}

impl Evaluatable for Unary {
    fn evaluate(&self, i: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let right = self.right.evaluate(i)?;
        if let TokenLiteralType::Number(right_num) = right {
            if TokenType::MINUS == self.operator.token_type {
                return Ok(TokenLiteralType::Number(-right_num));
            }
        }
        if TokenType::BANG == self.operator.token_type {
            return Ok(TokenLiteralType::Bool(!right.is_truthy()));
        }

        return Err(RuntimeError::new(self.operator.clone(), "Invalid operator"));
    }
}

impl Resolvable for Unary {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError>  {
        self.right.resolve(resolver)
    }
}

#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Variable {
    pub name: Token
}

impl Evaluatable for Variable {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        // println!("evaling {} with env {:?}", self.name.lexeme, interpreter.environment.borrow().values);
        // interpreter.environment.borrow().get(&self.name)
        interpreter.look_up_variable(&self.name, &Expr::Variable(Box::new(self.clone())))
    }
}

impl Resolvable for Variable {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        if let Some(scope) = resolver.scopes.last() {
            if let Some(res) = scope.get(&self.name.lexeme) {
                if *res == false {
                    return Err(ResolutionError::new(self.name.clone(), "Cannot read local variable in its own initializer."))
                }
            }
        }
        resolver.resolve_local(Expr::Variable(Box::new(self.clone())), self.name.clone())
    }
}

impl Display for Variable {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.name.lexeme)
    }
}


#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Assign {
    name: Token,
    value: Expr
}

impl Display for Assign {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.name.lexeme)
    }
}
impl Evaluatable for Assign {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let value = self.value.evaluate(interpreter)?;
        // interpreter.environment.borrow_mut().assign(&self.name, value.clone())?;
        let distance = interpreter.locals.get(&Expr::Assign(Box::new(self.clone())));
        match distance {
            Some(distance) => interpreter.environment.borrow_mut().assign_at(*distance, &self.name, value.clone())?,
            None => interpreter.global_environment.borrow_mut().assign(&self.name, value.clone())?,
        }
        Ok(value)
    }
}
impl Resolvable for Assign {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.value.resolve(resolver)?;
        resolver.resolve_local(Expr::Assign(Box::new(self.clone())), self.name.clone())?;
        Ok(())
    }
}

#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Logical {
    left: Expr,
    operator: Token,
    right: Expr,
}

impl Display for Logical {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.operator.lexeme)
    }
}

impl Evaluatable for Logical {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let left = self.left.evaluate(interpreter)?;
        match &self.operator.token_type {
            TokenType::OR => match left.is_truthy() {
                true => Ok(left),
                false => self.right.evaluate(interpreter),
            },
            TokenType::AND => match left.is_truthy() {
                true => self.right.evaluate(interpreter),
                false => Ok(left)
            }
            _ => Err(RuntimeError::new(self.operator.clone(), "Logical operator incorrect."))
        }
    }
}

impl Resolvable for Logical {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError>  {
        self.left.resolve(resolver)?;
        self.right.resolve(resolver)?;
        Ok(())
    }
}


#[derive(new, Debug, Clone, Hash, Eq, PartialEq)]
pub struct Call {
    callee: Expr,
    close_paren: Token,
    arguments: Vec<Expr>,
}

impl Display for Call {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "function")
    }
}


impl Evaluatable for Call {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError> {
        let callee = self.callee.evaluate(interpreter)?;
        let mut arguments = Vec::new();
        for arg in &self.arguments {
            arguments.push(arg.evaluate(interpreter)?);
        }
        match callee {
            TokenLiteralType::Callable(mut c) => {
                if arguments.len() != c.arity() {
                    return Err(RuntimeError::new(self.close_paren.clone(), format!("Expected {} arguments but received {}.", c.arity(), arguments.len())))
                }
                return c.call(interpreter, arguments);
            },
            _ => return Err(RuntimeError::new(self.close_paren.clone(), "Can only call callables."))
        }
    }
}

impl Resolvable for Call {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError> {
        self.callee.resolve(resolver)?;
        for arg in &self.arguments {
            arg.resolve(resolver)?;
        }
        Ok(())
    }
}



