
use crate::token_literal_type::TokenLiteralType;
use crate::stmt::Stmt;
use crate::environment::Environment;
use crate::token::Token;
use std::fmt::{Display, Formatter};
use std::fmt;
use std::rc::Rc;
use std::cell::{RefCell};
use std::time::{Duration, SystemTime};
use crate::callable::{Callable};
use crate::token_type::TokenType::SEMICOLON;
use std::collections::HashMap;
use crate::expr::Expr;

pub trait Evaluatable {
    fn evaluate(&self, interpreter: &mut Interpreter) -> Result<TokenLiteralType, RuntimeError>;
}

#[derive(Clone, Debug)]
pub struct RuntimeError {
    pub token: Token,
    pub err_message: String,
    pub return_value: Option<TokenLiteralType>,
}

impl RuntimeError {
    pub fn new(token: Token, err_message: impl Into<String>) -> Self {
        Self {
            token,
            err_message: err_message.into(),
            return_value: None
        }
    }
    pub fn new_return(token: Token, err_message: impl Into<String>, return_value: TokenLiteralType) -> Self {
        Self {
            token,
            err_message: err_message.into(),
            return_value: Some(return_value)
        }
    }
}

impl Display for RuntimeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Runtime Error: {} at {}", self.err_message, self.token.line)
    }
}

#[derive(Clone, Debug)]
pub struct Interpreter<> {
    pub global_environment: Rc<RefCell<Environment>>,
    pub environment: Rc<RefCell<Environment>>,
    pub locals: HashMap<Expr, usize>
}

impl<> Interpreter<> {
    pub fn new() -> Interpreter<> {
        let global_environment = Rc::new(RefCell::new(Environment::new(None)));
        let mut s = Self {
            global_environment: global_environment.clone(),
            environment: global_environment,
            locals: HashMap::new(),
        };
        s.define_globals();
        s
    }
    pub fn execute_block(&mut self, statements: &Vec<Stmt>, new_environment: Rc<RefCell<Environment>>) -> Result<(), RuntimeError> {
        let previous = self.environment.clone();
        self.environment = new_environment.clone();
        let interpret_res = self.interpret(statements);
        self.environment = previous.clone();
        interpret_res
    }
    pub fn define_globals(&mut self) {
        self.define_clock();
    }
    pub fn define_clock(&mut self) {

    }
    pub fn interpret(&mut self, statements: &Vec<Stmt>) -> Result<(), RuntimeError> {
        for stmt in statements {
            match stmt.evaluate(self) {
                Ok(_) => (),
                Err(e) => {
                    return Err(e);
                }
            };
        }
        Ok(())
    }
    pub fn resolve(&mut self, expr: Expr, depth: usize)  {
        self.locals.insert(expr, depth);
    }
    pub fn look_up_variable(&self, name: &Token, expr: &Expr) -> Result<TokenLiteralType, RuntimeError>  {
        let distance = self.locals.get(expr);
        if let Some(distance) = distance {
            self.environment.borrow().get_at(*distance, name)
        } else {
            self.global_environment.borrow().get(name)
        }
    }
}