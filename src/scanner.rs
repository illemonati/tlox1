use crate::token::{Token};
use crate::token_type::TokenType::*;
use crate::token_type::TokenType;
use crate::lox::Lox;
use crate::token_literal_type::TokenLiteralType;
use ordered_float::NotNan;

pub struct Scanner<'a> {
    source: String,
    tokens: Vec<Token>,
    start: usize,
    current: usize,
    line: usize,
    lox: &'a mut Lox,
}


impl<'a> Scanner<'a> {
    pub fn new(source: impl Into<String>, lox: &'a mut Lox) -> Scanner<'a> {
        Self {
            source: source.into(),
            tokens: Vec::new(),
            start: 0,
            current: 0,
            line: 1,
            lox,
        }
    }

    pub fn scan_tokens(&mut self) -> &Vec<Token> {
        while !self.is_at_end() {
            self.start = self.current;
            self.scan_token()
        }
        self.tokens.push(Token::new(EOF, "", TokenLiteralType::Nil, self.line));
        &self.tokens
    }

    fn is_at_end(&self) -> bool {
        self.current == self.source.len()
    }

    fn scan_token(&mut self) {
        let c = self.advance();
        match c {
            '(' => self.add_token(LEFT_PAREN, TokenLiteralType::Nil),
            ')' => self.add_token(RIGHT_PAREN, TokenLiteralType::Nil),
            '{' =>  self.add_token(LEFT_BRACE, TokenLiteralType::Nil),
            '}' => self.add_token(RIGHT_BRACE, TokenLiteralType::Nil),
            ',' => self.add_token(COMMA, TokenLiteralType::Nil),
            '.' => self.add_token(DOT, TokenLiteralType::Nil),
            '-' => self.add_token(MINUS, TokenLiteralType::Nil),
            '+' => self.add_token(PLUS, TokenLiteralType::Nil),
            ';' => self.add_token(SEMICOLON, TokenLiteralType::Nil),
            '*' => self.add_token(STAR, TokenLiteralType::Nil),
            '!' => self.next_match_add('=', BANG_EQUAL, BANG),
            '=' => self.next_match_add('=', EQUAL_EQUAL, EQUAL),
            '<' => self.next_match_add('=', LESS_EQUAL, LESS),
            '>' => self.next_match_add('=', GREATER_EQUAL, GREATER),
            '/' => {
                if self.next_match('/') {
                    while self.peek() != '\n' && !self.is_at_end() {
                        self.advance();
                    }
                } else {
                    self.add_token(SLASH, TokenLiteralType::Nil);
                }
            },
            ' ' => (),
            '\r' => (),
            '\t' => (),
            '\n' => self.line += 1,
            '"' => self.handle_string(),
            _  => {
                if c.is_digit(10) {
                    self.handle_number();
                } else if c.is_alphabetic() {
                    self.handle_identifier();
                } else {
                    self.lox.error(self.line, "Unexpected character.");
                }
            },
        }
    }

    fn handle_identifier(&mut self) {
        while self.peek().is_alphanumeric() {
            self.advance();
        }
        let text = &self.source[self.start..self.current];
        let token_type = match text {
            "and" => AND,
            "class" => CLASS,
            "else" => ELSE,
            "for" => FOR,
            "fun" => FUN,
            "if" => IF,
            "nil" => NIL,
            "or" => OR,
            "print" => PRINT,
            "return" => RETURN,
            "super" => SUPER,
            "this" => THIS,
            "true" => TRUE,
            "var" => VAR,
            "while" => WHILE,
            _ => IDENTIFIER,
        };
        self.add_token(token_type, TokenLiteralType::Nil);
    }

    fn handle_number(&mut self) {
        while self.peek().is_digit(10) {
            self.advance();
        }
        if self.peek() == '.' && self.peek_n(2).is_digit(10) {
            self.advance();
            while self.peek().is_digit(10) {
                self.advance();
            }
        }
        self.add_token(NUMBER, TokenLiteralType::Number(NotNan::new(self.source[self.start..self.current].parse::<f64>().unwrap()).unwrap()));
    }

    fn handle_string(&mut self) {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }
            self.advance();
        }
        if self.is_at_end() {
            self.lox.error(self.line, "Unterminated string.");
            return;
        }
        self.advance();
        let str_val = String::from(&self.source[self.start+1..self.current-1]);
        self.add_token(STRING, TokenLiteralType::String(str_val));
    }

    fn next_match_add(&mut self, expected: char, token1: TokenType, token2: TokenType) {
        if self.next_match(expected) {
            self.add_token(token1, TokenLiteralType::Nil);
        } else {
            self.add_token(token2, TokenLiteralType::Nil);
        }
    }

    fn peek(&self) -> char {
        if self.is_at_end() {
            '\0'
        } else {
            self.source.char_indices().nth(self.current).unwrap().1
        }
    }

    fn peek_n(&self, n: usize) -> char {
        let index = self.current + n - 1;
        if index == self.source.len() {
            '\0'
        } else {
            self.source.char_indices().nth(index).unwrap().1
        }
    }

    fn next_match(&mut self, expected: char) -> bool {
        if self.is_at_end() {
            return false;
        }
        if self.source.char_indices().nth(self.current).unwrap().1 != expected {
            return false;
        }
        self.current += 1;
        true
    }

    fn advance(&mut self) -> char {
        self.current += 1;
        self.source.char_indices().nth(self.current-1).unwrap().1
    }

    fn add_token(&mut self, token_type: TokenType, literal: TokenLiteralType) {
        // if literal != TokenLiteralType::Nil {
        let text = &self.source[self.start..self.current];
        // }
        self.tokens.push(Token::new(token_type, text, literal, self.line));
    }
}



