use crate::token::{Token};
use crate::token_literal_type::TokenLiteralType;
use crate::expr::{Expr, Literal, Grouping, Binary, Unary, Variable, Assign, Logical, Call};
use crate::TokenType::*;
use crate::token_type::TokenType;
use crate::lox::Lox;
use std::fmt::{Display, Formatter};
use std::fmt;
use crate::stmt::{Stmt, Print, Expression, Var, Block, If, While, Function, Return};

#[derive(Clone, Debug)]
pub struct ParserError {
    pub token: Token,
    pub err_message: String,
}

impl ParserError {
    pub fn new(token: Token, err_message: impl Into<String>) -> Self {
        Self {
            token,
            err_message: err_message.into(),
        }
    }
}

impl Display for ParserError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Parser Error")
    }
}


pub struct Parser<'a> {
    token: Vec<Token>,
    current: usize,
    lox: &'a mut Lox,
}

impl<'a> Parser<'a> {
    pub fn new(token: Vec<Token>, lox: &'a mut Lox) -> Parser<'a> {
        Self {
            token,
            current: 0,
            lox
        }
    }
    fn expression(&mut self) -> Result<Expr, ParserError> {
        self.assignment()
    }

    fn assignment(&mut self) -> Result<Expr, ParserError> {
        let expr: Expr = self.or()?;
        if self.match_type(&[EQUAL]) {
            let equals = self.previous().clone();
            let value = self.assignment()?;
            if let Expr::Variable(expr) = expr.clone() {
                let name = expr.name.clone();
                return Ok(Expr::Assign(Box::new(Assign::new(name, value))));
            }
            self.error(&equals, "Invalid assignment target.");
        }
        Ok(expr)
    }

    fn or(&mut self) -> Result<Expr, ParserError> {
        let mut expr: Expr = self.and()?;
        while self.match_type(&[OR]) {
            let operator = self.previous().clone();
            let right = self.and()?;
            expr = Expr::Logical(Box::new(Logical::new(expr.clone(), operator, right)));
        }
        Ok(expr)
    }

    fn and(&mut self) -> Result<Expr, ParserError> {
        let mut expr: Expr = self.equality()?;
        while self.match_type(&[AND]) {
            let operator = self.previous().clone();
            let right = self.equality()?;
            expr = Expr::Logical(Box::new(Logical::new(expr.clone(), operator, right)));
        }
        Ok(expr)
    }

    fn equality(&mut self) -> Result<Expr, ParserError> {
        let mut expr: Expr = self.comparison()?;
        while self.match_type(&[BANG_EQUAL, EQUAL_EQUAL]) {
            let operator = self.previous().clone();
            let right = self.comparison()?;
            expr = Expr::Binary(Box::new(Binary::new(expr.clone(), operator, right)));
        }
        Ok(expr)
    }
    fn comparison(&mut self) -> Result<Expr, ParserError> {
        let mut expr: Expr = self.addition()?;
        while self.match_type(&[GREATER, GREATER_EQUAL, LESS, LESS_EQUAL]) {
            let operator = self.previous().clone();
            let right = self.addition()?;
            expr = Expr::Binary(Box::new(Binary::new(expr.clone(), operator, right)));
        }
        Ok(expr)
    }
    fn addition(&mut self) -> Result<Expr, ParserError> {
        let mut expr: Expr = self.multiplication()?;
        while self.match_type(&[MINUS, PLUS]) {
            let operator = self.previous().clone();
            let right = self.multiplication()?;
            expr = Expr::Binary(Box::new(Binary::new(expr.clone(), operator, right)));
        }
        Ok(expr)
    }
    fn multiplication(&mut self) -> Result<Expr, ParserError>  {
        let mut expr: Expr = self.unary()?;
        while self.match_type(&[SLASH, STAR]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            expr = Expr::Binary(Box::new(Binary::new(expr.clone(), operator, right)));
        }
        Ok(expr)
    }
    fn unary(&mut self) -> Result<Expr, ParserError>  {
        if self.match_type(&[BANG, MINUS]) {
            let operator = self.previous().clone();
            let right = self.unary()?;
            return Ok(Expr::Unary(Box::new(Unary::new(operator.clone(), right))));
        }
        return self.call();
    }
    fn call(&mut self) -> Result<Expr, ParserError> {
        let mut expr: Expr = self.primary()?;
        loop {
            if self.match_type(&[LEFT_PAREN]) {
                expr = self.finish_call(expr)?;
            } else {
                break;
            }
        }
        Ok(expr)
    }
    fn primary(&mut self) -> Result<Expr, ParserError> {
        if self.match_type(&[FALSE]) {
            Ok(Expr::Literal(Box::new(Literal::new(TokenLiteralType::Bool(false)))))
        } else if self.match_type(&[TRUE]) {
            Ok(Expr::Literal(Box::new(Literal::new(TokenLiteralType::Bool(true)))))
        } else if self.match_type(&[NIL]) {
            Ok(Expr::Literal(Box::new(Literal::new(TokenLiteralType::Nil))))
        } else if self.match_type(&[NUMBER, STRING]) {
            Ok(Expr::Literal(Box::new(Literal::new(self.previous().clone().literal))))
        } else if self.match_type(&[LEFT_PAREN]) {
            let expr = self.expression()?;
            match self.consume(&RIGHT_PAREN, "Expect ')' after expression.") {
                Ok(_) => Ok(Expr::Grouping(Box::new(Grouping::new(expr)))),
                Err(e) => Err(e)
            }
        } else if self.match_type(&[IDENTIFIER]) {
            Ok(Expr::Variable(Box::new(Variable::new(self.previous().clone()))))
        } else {
            Err(ParserError::new(self.peek().clone(), String::from("Expect expression.")))
        }
    }

    fn finish_call(&mut self, callee: Expr) -> Result<Expr, ParserError> {
        let mut args: Vec<Expr> = Vec::new();
        if !self.check_type(&RIGHT_PAREN) {
            loop {
                if args.len() > 255 {
                    return Err(ParserError::new(self.peek().clone(), "Cannot have more than 255 arguments."))
                }
                args.push(self.expression()?);
                if !self.match_type(&[COMMA]) {
                    break;
                }
            }
        }
        let right_paren = self.consume(&RIGHT_PAREN, "Expected ) after arguments.")?;
        Ok(Expr::Call(Box::new(Call::new(callee, right_paren.clone(), args))))
    }


    fn synchronize(&mut self) {
        self.advance();
        while !self.is_at_end() {
            if self.previous().token_type == SEMICOLON {
                break;
            }
            let list = [
                CLASS,
                FUN,
                VAR,
                FOR,
                IF,
                WHILE,
                PRINT,
                RETURN,
            ];
            if list.contains(&self.peek().token_type) {
                break;
            }
            self.advance();
        }
    }

    fn consume(&mut self, token_type: &TokenType, err_message: &str) -> Result<&Token, ParserError> {
        if self.check_type(&token_type) {
            Ok(self.advance())
        } else {
            Err(ParserError::new(self.peek().clone(), String::from(err_message)))
        }
    }
    fn error(&mut self, token: &Token, err_message: &str) {
        self.lox.token_err(token, err_message);
    }
    fn peek(&self) -> &Token {
        &self.token[self.current]
    }
    fn previous(&self) -> &Token {
        // println!("{}", &self.token[self.current-1]);
        &self.token[self.current-1]
    }
    fn is_at_end(&self) -> bool {
        self.peek().token_type == EOF
    }
    fn advance(&mut self) -> &Token {
        if !self.is_at_end() {
            self.current += 1;
        }
        self.previous()
    }
    fn check_type(&mut self, token_type: &TokenType) -> bool  {
        if self.is_at_end() {
            false
        } else {
            self.peek().token_type == token_type.clone()
        }
    }
    fn match_type(&mut self, types: &[TokenType]) -> bool {
        for token_type in types {
            if self.check_type(token_type) {
                self.advance();
                return true;
            }
        }
        false
    }

    fn print_statement(&mut self) -> Result<Stmt, ParserError> {
        let expr = self.expression()?;
        self.consume(&SEMICOLON, "Expect ';' after value.")?;
        Ok(Stmt::Print(Box::new(Print::new(expr))))
    }

    fn expr_statement(&mut self) -> Result<Stmt, ParserError> {
        let expr = self.expression()?;
        self.consume(&SEMICOLON, "Expect ';' after value.")?;
        Ok(Stmt::Expression(Box::new(Expression::new(expr))))
    }

    fn block(&mut self) -> Result<Vec<Stmt>, ParserError> {
        let mut statements = Vec::new();
        while !self.check_type(&RIGHT_BRACE) && !self.is_at_end() {
            statements.push(self.declaration()?);
        }
        self.consume(&RIGHT_BRACE, "Expect '}' after block.")?;
        Ok(statements)
    }

    fn if_statement(&mut self) -> Result<Stmt, ParserError> {
        self.consume(&LEFT_PAREN, "Expected '(' after if.")?;
        let condition = self.expression()?;
        self.consume(&RIGHT_PAREN, "Expected ')' after condition.")?;
        let then_branch = self.statement()?;
        let else_branch = match self.match_type(&[ELSE]) {
            true => self.statement()?,
            false => Stmt::Expression(Box::new(Expression::new(Expr::Literal(Box::new(Literal::new(TokenLiteralType::Nil))))))
        };
        return Ok(Stmt::If(Box::new(If::new(condition, then_branch, else_branch))))
    }

    fn while_statement(&mut self) -> Result<Stmt, ParserError> {
        self.consume(&LEFT_PAREN, "Expect '(' after 'while'.")?;
        let condition = self.expression()?;
        self.consume(&RIGHT_PAREN, "Expect ')' after condition.")?;
        let body = self.statement()?;
        Ok(Stmt::While(Box::new(While::new(condition, body))))
    }

    fn for_statement(&mut self) -> Result<Stmt, ParserError> {
        self.consume(&LEFT_PAREN, "Expect '(' after 'for'.")?;
        let mut initializer = None;
        if self.match_type(&[VAR]) {
            initializer = Some(self.var_declaration()?);
        } else if !self.match_type(&[SEMICOLON]) {
            initializer = Some(self.expr_statement()?);
        }
        let condition = match self.check_type(&SEMICOLON) {
            true => None,
            false => Some(self.expression()?),
        };
        self.consume(&SEMICOLON, "Expect ';' after loop condition.")?;
        let increment: Option<Stmt> = match self.check_type(&RIGHT_PAREN) {
            true => None,
            false => Some(Stmt::Expression(Box::new(Expression::new(self.expression()?)))),
        };
        self.consume(&RIGHT_PAREN, "Expect ')' after for clauses.")?;
        let mut body = self.statement()?;


        // desurger the loop conditions
        if let Some(increment) = increment {
            body = Stmt::Block(Box::new(Block::new(vec![body, increment])));
        }

        match condition {
            Some(con) => {
                body = Stmt::While(Box::new(While::new(con, body)));
            },
            None => {
                body = Stmt::While(Box::new(While::new(Expr::Literal(Box::new(Literal::new(TokenLiteralType::Bool(true)))), body)));
            }
        }

        if let Some(initializer) = initializer {
            body = Stmt::Block(Box::new(Block::new(vec![initializer, body])));
        }
        Ok(body)
    }

    fn fun_statement(&mut self, kind: &str) -> Result<Stmt, ParserError> {
        let name = self.consume(&IDENTIFIER, &format!("Expect {} name.", &kind))?.clone();
        self.consume(&LEFT_PAREN, &format!("Expect '(' after {} name.", &kind))?;
        let mut params = Vec::new();
        if !self.check_type(&RIGHT_PAREN) {
            loop {
                if params.len() >= 255 {
                    return Err(ParserError::new(self.peek().clone(), "Cannot have more than 255 parameters."));
                }
                params.push(self.consume(&IDENTIFIER, "Expect parameter name.")?.clone());
                if !self.match_type(&[COMMA]) {
                    break;
                }
            }
        }
        self.consume(&RIGHT_PAREN, "Expect ')' after parameters.")?;
        self.consume(&LEFT_BRACE, &format!("Expect '{}' before {} body", "{", &kind))?;
        let body = self.block()?;
        Ok(Stmt::Function(Box::new(Function::new(name, params, body))))
    }

    fn return_statement(&mut self) -> Result<Stmt, ParserError> {
        let keyword = self.previous().clone();
        let mut value = Expr::Literal(Box::new(Literal::new(TokenLiteralType::Nil)));
        if !self.check_type(&SEMICOLON) {
            value = self.expression()?.clone();
        }
        self.consume(&SEMICOLON, "Expect ';' after return value.");
        Ok(Stmt::Return(Box::new(Return::new(keyword, value))))
    }

    fn statement(&mut self) -> Result<Stmt, ParserError> {
        if self.match_type(&[PRINT]) {
            return self.print_statement();
        }
        if self.match_type(&[LEFT_BRACE]) {
            return Ok(Stmt::Block(Box::new(Block::new(self.block()?))));
        }
        if self.match_type(&[IF]) {
            return self.if_statement();
        }
        if self.match_type(&[WHILE]) {
            return self.while_statement();
        }
        if self.match_type(&[FOR]) {
            return self.for_statement();
        }
        if self.match_type(&[FUN]) {
            return self.fun_statement("function");
        }
        if self.match_type(&[RETURN]) {
            return self.return_statement();
        }
        return self.expr_statement();
    }

    fn var_declaration(&mut self) -> Result<Stmt, ParserError> {
        let name = self.consume(&IDENTIFIER, "Expect variable name.")?.clone();
        let mut initializer = Expr::Literal(Box::new(Literal::new(TokenLiteralType::Nil)));
        if self.match_type(&[EQUAL]) {
            initializer = self.expression()?;
        }
        self.consume(&SEMICOLON, "Expect ';' after variable declaration.")?;
        Ok(Stmt::Var(Box::new(Var::new(name, initializer))))
    }

    fn declaration(&mut self) -> Result<Stmt, ParserError> {
        if self.match_type(&[VAR]) {
            self.var_declaration()
        } else {
            self.statement()
        }
    }


    pub fn parse(&mut self) -> Result<Vec<Stmt>, ParserError> {
        let mut statements: Vec<Stmt> = Vec::new();
        while !self.is_at_end() {
            match self.declaration() {
                Ok(stmt) => statements.push(stmt),
                Err(e) => {
                    // self.error(&e.token, &e.err_message);
                    self.synchronize();
                    return Err(e);
                }
            }
        }
        Ok(statements)
    }

}

