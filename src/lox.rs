use std::{fs, io, process};
use crate::scanner::Scanner;
use crate::parser::Parser;
use std::io::Write;
use crate::token::Token;
use crate::token_type::TokenType;
use crate::interpreter::{Interpreter};
use crate::resolver::Resolver;


#[derive(Clone)]
pub struct Lox{
    had_error: bool,
}

impl Lox {
    pub fn new() -> Self {
        Self {
            had_error: false
        }
    }
    pub fn run_file(&mut self, filepath: &str) {
        let file = fs::read_to_string(filepath).expect("Not a valid file");
        let mut interpreter = Interpreter::new();
        self.run(&file,  &mut interpreter);
        if self.had_error {
            process::exit(65);
        }
    }

    pub fn run_prompt(&mut self) {
        println!("tlox1");
        let mut interpreter = Interpreter::new();
        loop {
            print!("> ");
            let _ = io::stdout().flush();
            let mut line = String::new();
            match io::stdin().read_line(&mut line) {
                Ok(_) => {
                    self.run(&line, &mut interpreter);
                }
                Err(_) => break,
            }
            self.had_error = false;
        }
    }

    fn run(&mut self, program_text: &str, interpreter: &mut Interpreter) {
        let mut scanner = Scanner::new(program_text, self);
        // println!("{}", program_text);
        let tokens = scanner.scan_tokens();
        // for token in tokens {
        //     println!("{}", token);
        // }
        let mut parser = Parser::new(tokens.clone(), self);
        let stmts = parser.parse();
        if let Err(e) = stmts {
            self.token_err_with_type("Parse Error", &e.token, &e.err_message);
            return;
        }
        let stmts = stmts.unwrap();
        let mut resolver = Resolver::new(interpreter);
        if let Err(e) = resolver.resolve_block(&stmts) {
            self.token_err_with_type("Resolution Error", &e.token, &e.err_message);
            return;
        };
        let res = interpreter.interpret(&stmts);
        if let Err(e) = res {
            self.token_err_with_type("Runtime Error", &e.token, &e.err_message);
            return;
        }
    }

    pub fn error(&mut self, line: usize, message: &str) {
        self.report(line, "", message);
    }

    pub fn token_err(&mut self, token: &Token, message: &str) {
        match token.token_type {
            TokenType::EOF => self.report(token.line, " at end", message),
            _ => self.report(token.line, &format!(" at '{}'", token.lexeme), message)
        }
    }

    pub fn token_err_with_type(&mut self, err_type: &str, token: &Token, message: &str) {
        match token.token_type {
            TokenType::EOF => self.report(token.line, " at end", message),
            _ => self.report(token.line, &format!(" at '{}', {} ", token.lexeme, err_type), message)
        }
    }

    fn report(&mut self, line: usize, place: &str, message: &str) {
        eprintln!("[line {}] Error{}: {}", line, place, message);
        self.had_error = true;
    }

}