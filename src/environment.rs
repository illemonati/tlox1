use std::collections::HashMap;
use crate::token_literal_type::TokenLiteralType;
use crate::token::Token;
use crate::interpreter::RuntimeError;
use std::cell::RefCell;
use std::rc::Rc;
use std::hash::{Hash, Hasher};
use crate::token_type::TokenType;

#[derive(Debug, Eq)]
pub struct Environment{
    pub values: HashMap<String, TokenLiteralType>,
    pub enclosing: Option<Rc<RefCell<Environment>>>
}

impl Hash for Environment {
    fn hash<H: Hasher>(&self, state: &mut H) {
        format!("{:?}", self.values).hash(state);
    }
}

impl PartialEq for Environment {
    fn eq(&self, other: &Self) -> bool {
        self.values == other.values
    }
}




impl Environment {
    pub fn new(enclosing: Option<Rc<RefCell<Environment>>>) -> Self {
        Environment {
            values: HashMap::new(),
            enclosing
        }
    }
    pub fn define(&mut self, name: impl Into<String>, value: TokenLiteralType) {
        self.values.insert(name.into(), value);
    }
    pub fn get(&self, name: &Token) -> Result<TokenLiteralType, RuntimeError>  {
        if let Some(var) = self.values.get(&name.lexeme) {
            return Ok(var.clone());
        }

        if let Some(e) = self.enclosing.clone() {
            return e.borrow().get(name);
        }
        Err(RuntimeError::new(name.clone(), format!("Undefined variable '{}'.", &name.lexeme)))
    }
    pub fn assign(&mut self, name: &Token, value: TokenLiteralType) -> Result<(), RuntimeError> {
        if self.values.contains_key(&name.lexeme) {
            *self.values.get_mut(&name.lexeme).unwrap() = value;
            return Ok(())
        }
        if let Some(enclosing) = self.enclosing.clone() {
            return enclosing.borrow_mut().assign(name, value);
        }
        Err(RuntimeError::new(name.clone(), format!("Undefined variable '{}'.", name.lexeme)))
    }
    pub fn get_at(&self, distance: usize, token: &Token) -> Result<TokenLiteralType, RuntimeError>  {
        if distance == 0 {
            return match self.values.get(&token.lexeme) {
                Some(val) => Ok(val.clone()),
                None => Err(RuntimeError::new(token.clone(), "Error Looking up variable."))
            }
        }
        let ancestor = self.ancestor(distance);
        match ancestor {
            Some(ancestor) => {
                ancestor.borrow().get(token)
            },
            None => {
                Err(RuntimeError::new(token.clone(), "Error resolving ancestor."))
            }
        }
    }
    pub fn assign_at(&mut self, distance: usize, token: &Token, value: TokenLiteralType) -> Result<(), RuntimeError>{
        if distance == 0 {
            self.assign(token, value)
        } else {
            match self.ancestor(distance) {
                Some(ancestor) => {
                    ancestor.borrow_mut().assign(token, value)
                },
                None => Err(RuntimeError::new(token.clone(), "Error resolving ancestor."))
            }
        }
    }
    pub fn ancestor(&self, distance: usize) -> Option<Rc<RefCell<Environment>>> {
        if distance == 0 {
            return None;
        }
        let mut ancestor = match self.enclosing.clone() {
            Some(a) => a,
            None => return None,
        };
        for i in 1..distance {
            let temp = ancestor.clone();
            ancestor = match temp.clone().borrow().enclosing.clone() {
                Some(a) => a,
                None => return None
            }
        }
        Some(ancestor)
    }
}

