use crate::interpreter::{Interpreter, RuntimeError};
use crate::token_literal_type::TokenLiteralType;
use crate::token::Token;
use crate::stmt::{Stmt, Function};
use crate::expr::Expr;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct ResolutionError {
    pub token: Token,
    pub err_message: String,
}

impl ResolutionError {
    pub fn new(token: Token, err_message: impl Into<String>) -> Self {
        Self {
            token,
            err_message: err_message.into(),
        }
    }
}

pub trait Resolvable {
    fn resolve(&self, resolver: &mut Resolver) -> Result<(), ResolutionError>;
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum FunctionType {
    Function,
    None
}

#[derive(Debug)]
pub struct Resolver<'a> {
    pub interpreter: &'a mut Interpreter,
    pub scopes: Vec<HashMap<String, bool>>,
    pub current_function_type: FunctionType,
}


impl<'a> Resolver<'a> {
    pub fn new(interpreter: &'a mut Interpreter) -> Self {
        Self {
            interpreter,
            scopes: Vec::new(),
            current_function_type: FunctionType::None
        }
    }
    pub fn resolve_block(&mut self, stmts: &Vec<Stmt>) -> Result<(), ResolutionError> {
        for stmt in stmts {
            stmt.resolve(self)?;
        }
        Ok(())
    }
    pub fn begin_scope(&mut self) {
        self.scopes.push(HashMap::new());
    }
    pub fn end_scope(&mut self) {
        self.scopes.pop();
    }
    pub fn declare(&mut self, name: &Token) -> Result<(), ResolutionError> {
        if let Some(scope) = self.scopes.last_mut() {
            if scope.contains_key(&name.lexeme) {
                return Err(ResolutionError::new(name.clone(), "Variable with this name already declared in this scope."))
            }
            scope.insert(name.lexeme.clone(), false);
        }
        Ok(())
    }
    pub fn define(&mut self, name: &Token) {
        if let Some(scope) = self.scopes.last_mut() {
            scope.insert(name.lexeme.clone(), true);
        }
    }
    pub fn resolve_local(&mut self, expr: Expr, name: Token) -> Result<(), ResolutionError> {
        for i in (0..self.scopes.len()).rev() {
            if let Some(scope) = self.scopes.get(i) {
                if scope.contains_key(&name.lexeme) {
                    self.interpreter.resolve(expr.clone(), self.scopes.len()-1-i);
                    return Ok(())
                }
            }
        }
        Ok(())
    }
    pub fn resolve_function(&mut self, function: Function, function_type: FunctionType) -> Result<(), ResolutionError> {
        self.begin_scope();
        let enclosing_function_type = self.current_function_type.clone();
        self.current_function_type = function_type;
        for param in function.params {
            self.declare(&param)?;
            self.define(&param);
        }
        self.resolve_block(&function.body)?;
        self.end_scope();
        self.current_function_type = enclosing_function_type;
        Ok(())
    }
}

